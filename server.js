const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

const bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);
console.log("API escuchando en el puerto "+port);

app.get('/apitechu/v1/hello',
  function(req,res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Hola desde API TechU" });
  }
);

/*app.get('/apitechu/v1/users',
  function(req,res){
    console.log("GET /apitechu/v1/users");
    //res.sendFile('usuarios.json',{root: __dirname});
    var users = require('./usuarios.json');
    res.send(users);
  }
);
*/
app.post('/apitechu/v1/users',
  function(req,res){
    console.log("POST /apitechu/v1/users");
    console.log("First name es : " + req.body.first_name);
    console.log("Last name es : " + req.body.last_name);
    console.log("Email es : " + req.body.email);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
    };

    var users = require('./usuarios.json');
    users.push(newUser);

    writeUserDataToFile(users);

    console.log("Usuario añadido con exito");
    res.send({"msg" : "Usuario añadido con exito" });

    //res.send(users); //Se devuelve el listado para comprobar que se ha insertado

  }
);

app.delete('/apitechu/v1/users/:id',
  function(req,res){
    console.log("DELETE /apitechu/v1/users/:id");
    var idFind = req.params.id;
    console.log("id es "+idFind);
    var users = require('./usuarios.json');

    // SOLUCION 1
/*    for (user of users){
      console.log("Length of array is "+users.length);
      if (user != null && user.id == idFind){
        console.log("La ID coincide");
        //delete users[user.id - 1];
        users.splice(user.id - 1, 1);
        break;
      }
    }
*/    // SOLUCION 2
    for (arrayID in users) {
        var id_X = users[arrayID].id;
        var name_X = users[arrayID].first_name;
        if (id_X == idFind){
          console.log("Encontrado el nombre del id "+id_X+" es "+name_X);
          // Con splice
          //users.splice(arrayID, 1);
          // Con delete
          delete users[arrayID];
          break;
        }
    }

    // SOLUCION 3
/*    users.forEach(function(user,index){
      if (user.id==idFind){
        console.log("La ID coincide");
        users.splice(index, 1);
      }
    })
*/
    console.log("Usuario con id "+id_X+" borrado con exito");
    writeUserDataToFile(users);
    res.send({"msg" : "Usuario con id "+id_X+" borrado con exito" });
  }
);

function writeUserDataToFile(data){
  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err){
      if (err){
        console.log(err);
      }else{
        console.log("Datos escritos en fichero");
      }
    });

}


app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
    console.log("POST apitechu/v1/monstruo/:p1/:p2");
    console.log("Parametros");
    console.log(req.params);
    console.log("Query String");
    console.log(req.query);
    console.log("Headers");
    console.log(req.headers);
    console.log("Body");
    console.log(req.body);
  }
);


/*
app.get('/apitechu/v2/users/',
  function(req,res){
    console.log("GET /apitechu/v1/users");
    var filtro1 = req.query.top;
    console.log("Filtro 1 = "+filtro1);
    var filtro2 = req.query.count;
    console.log("Filtro 2 = "+filtro2);

    var respuesta = {};

    var users = require('./usuarios.json');
    var longitudUsers = users.length;
    if (filtro2){
      console.log("Longitud = "+longitudUsers);
      respuesta.push({
        "count" : longitudUsers,
        "listaFiltrada" : users.splice(0, filtro1);
      });
    }

//   if (filtro1>0){
//      respuesta += users.splice(0, filtro1);
//    }

    res.send(respuesta);
  }
);
*/
app.post("/apitechu/v1/login/",
  function(req,res){
    console.log("POST apitechu/v1/login/");
    //console.log("Body");
    var usuarioLoginEmail = req.body.email;
    //console.log("Usuario = "+usuarioLoginEmail);
    var usuarioPwdEmail = req.body.password;
    //console.log("Password = "+usuarioPwdEmail);

    var users = require('./seguridadUsuarios.json');
    var result = {};

    var encontrado = false;
    for (user of users){
      //console.log("Usuario : "+user.email);
      if (user != null && user.email == usuarioLoginEmail){
        //console.log("Usuario Encontrado");
        encontrado = true;
        if (user.password == usuarioPwdEmail){
          //console.log("Password SÍ coincide");
          user.logged = true;
          result.idUsuario = user.id;
          result.mensaje = "LOGIN correcto";
        }else{
          //console.log("Password NO coincide");
          result.mensaje = "LOGIN incorrecto -> Password NO coincide";
        }
        break;
      }
    }
    if (!encontrado){
      //console.log("Usuario NO Encontrado");
      result.mensaje = "LOGIN incorrecto -> Usuario NO Encontrado";
    }

    //console.log("Finalizado");
    writeUserDataToFile2(users);
    res.send(result);
  }
);

app.post("/apitechu/v1/logout/",
  function(req,res){
    console.log("POST apitechu/v1/logout/");
    //console.log("Body");
    var usuarioLogoutId = req.body.id;
    console.log("Usuario ID = "+usuarioLogoutId);

    var users = require('./seguridadUsuarios.json');
    var result = {};

    var encontrado = false;
    for (user of users){
      //console.log("Usuario : "+user.email);
      if (user != null && user.id == usuarioLogoutId){
        //console.log("Usuario Encontrado");
        encontrado = true;
        if (user.logged){
          //console.log("Usuario estaba conectado");
          delete user.logged;
          result.idUsuario = user.id;
          result.mensaje = "LOGOUT correcto";
        }else{
          //console.log("Usuario NO estaba conectado");
          result.mensaje = "LOGOUT incorrecto -> Usuario NO estaba conectado";
        }
        break;
      }
    }
    if (!encontrado){
      //console.log("Usuario NO Encontrado");
      result.mensaje = "LOGOUT incorrecto -> Usuario NO Encontrado";
    }

    //console.log("Finalizado");
    writeUserDataToFile2(users);
    res.send(result);
  }
);

function writeUserDataToFile2(data){
  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./seguridadUsuarios.json", jsonUserData, "utf8",
    function(err){
      if (err){
        console.log(err);
      }else{
        console.log("Datos escritos en fichero");
      }
    });

}
